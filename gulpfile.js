'use strict'

//Carga de plugins
const gulp = require("gulp");
const sass = require("gulp-sass");
const browsersync = require("browser-sync").create();
const del = require("del");
const imagemin = require("gulp-imagemin");
const uglify = require("gulp-uglify");
const usemin = require("gulp-usemin");
const rev = require("gulp-rev");
const cleanCss = require("gulp-clean-css");
const flatmap = require("gulp-flatmap");
const htmlmin = require("gulp-htmlmin");

function css() {
    return gulp.src('./css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));
}

function watchFiles() {
    gulp.watch('./css/*.scss', css);
    /*gulp.watch([
        './*.html',
        './css/*.css',
        './img/*.{png, jpg, gif}',
        './js/*.js'
    ], browserSyncReload);*/
}

function browserSync(done) {
    var files = ['./*.html', './css/*.css', './img/*.{png, jpg, gif}', './js/*.js']
    browsersync.init(files,{
        server: {
            baseDir: './'
        },
        port: 3000
    });
    done();
}

function browserSyncReload(done) {
    browsersync.reload();
    done();
}

function clean() {
    return del(['dist']);
}

function images() {
    return gulp.src('./images/*.{png,jpg,jpeg,gif}')
        .pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
        .pipe(gulp.dest('dist/images'));
}

function useMin() {
    return gulp.src('./*.html')
        .pipe(flatmap(function(stream, file){
            return stream
                .pipe(usemin({
                    css: [rev()],
                    html: [function() { return htmlmin({collapseWhitespace: true})}],
                    js: [uglify(), rev()],
                    inlinejs: [uglify()],
                    inlinecss: [cleanCss(), 'concat']
                }));
        }))
        .pipe(gulp.dest('dist/'));
}

function copyfonts() {
    return gulp.src('./node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot,otf}*')
        .pipe(gulp.dest('./dist/fonts'));
}

const watch = gulp.parallel(watchFiles, browserSync);
//const build = gulp.series(clean, copyfonts, images, useMin);
const build = gulp.series(clean, gulp.parallel(copyfonts, images, useMin));
exports.default = watch;
exports.clean = clean;
exports.images = images;
exports.usemin = useMin;
exports.copyfonts = copyfonts;
exports.build = build;
